# Project : DeezPlay

- Julien Besançon
- Maxime Rua
- Paul-Arthur Dalbis

## Extra package utilisé :
`**Picasso**` : Librairie pour récupérer et afficher des images à partir d'une url.

## Requirements
- Une connexion à internet **sans proxy**.

## Objectif

L'objectif de ce projet est de manipuler :
- les AsyncTask
- une bdd locale SQLite
- les listeviews associées à des customArrayAdapter

Le tout sous environnement Android.

## OverView

Lorsque l'application DeezPlay est lancée, l'utilisateur est invité à se logger si il a déjà un compte, et sinon créer un compte. Afin d'effectuer des tests fonctionnels, plusieurs utilisateurs sont préenregistrés dans la base de données à l'ouverture de l'application.

Utilisateurs :

  - Loggin : pa
  - Mdp : a


  - Loggin : ju
  - Mdp : ju

### Création d'un compte

Lors de la création d'un compte, l'utilisateur doit renseigner les champs suivants :
- `Identifiant` : son identifiant Deezer. Il est utilisé par Deezer pour identifier le profil utilisateur (e.g `https://www.deezer.com/fr/profile/12855440`, l'identifiant est `12855440`).
- `Login` : le username de l'utilisateur dans l'application.
- `Password` : le password de l'utilisateur pour l'application.
- `Confirmer` : pour confirmer le password

### Activité `Ecran de connexion`

L'utilisateur se connecte avec ses identifiants de l'application. Une fois connectée, ses playlists sont chargées depuis la base de données SQLite locale. L'identifiant Deezer est passé dans la nouvelle activity :
```java
intent.putExtra("id_deezer", idUser);
```
Et est récupéré dans la nouvelle activity :
```java
Intent intent = getIntent ();
this.id_deezer = intent.getStringExtra("id_deezer");
```

### Activité `Playlists`

Une fois connecté, les playlist de l'utilisateur sont chargées depuis la base de données SQLite locale et affichées par le moyen d'une custom ListView. L'utilisateur a l'option de mettre à jour ces playlists grâce au bouton `Rafraîchir vos playlists`.

Ce bouton enclanche une DeezerPlaylistAsynctask qui effectue une requête vers l'API Deezer en utilisant l'identifiant Deezer passé par l'intent. Puis :  
- Le JSON des playlists est récupéré, transformé en ArrayList de Playlist
- La base de données est mise à jour en fonction du résultat obtenu par cette requête. Ce résultat prévaut à la base de données locale. Cette dernière est donc mise à jour en conséquence (ajout/modification/suppression).
- La ListView est ensuite mise à jour.


Lorsque l'utilisateur sélectionne une playlist, cela l'envoie vers une nouvelle activity qui liste les musiques de cette playlist.

### Activité `Musiques`

Cette Activité affiche la liste des musiques qui sont associées à une playlist. Les musiques de l'utilisateur sont chargées depuis la base de données SQLite locale et affichées par le moyen d'une custom ListView. L'utilisateur a l'option de mettre à jour la liste de musiques grâce au bouton `Rafraîchir vos musiques`.  

Ce bouton enclanche une DeezerMusicAsynctask qui effectue les mêmes actions que l'AsyncTaks des playlists pour les musiques.  

Lorsque l'utilisateur sélectionne une musique, il lui est proposé d'ouvrir l'application Deezer ou un navigateur internet. Si le navigateur internet est sélectionné, le site Deezer propose uniquement de télécharger l'application Android Deezer (nous pensions que cela nous ouvrirait une page vers la page Deezer de la musique). Il faut donc sélectionner de lancer l'application Deezer. L'application s'ouvre et la musique peut être jouée.

## Difficultées rencontrées
- Lorsque une ListView est initialement vide, le `setOnItemClickListener()` n'est lié à rien, nous avons du réappliquer un `OnItemClickListener` dans le `onPostExecute()` de l'asynctask qui rafraichit les ListView et la Base de données.
- Si on ouvre une musique en sélectionnant un navigateur web, le site Deezer ne propose que de télécharger l'application android et n'affiche pas la musique. Il faut donc télécharger l'application Deezer et se logger un compte qui peux être indépendant des comptes enregistrés dans l'application DeezPlay. Ce problème en fin de développement.


## Améliorations possibles
- Factorisation des AsyncTask dont le `doInBackGround()` est identique.
- Stockage des images dans la base de données SQLite sous forme d'un String en base 64 en les récupérant grâce à nouvelle Asynctask qui nous retournerait une image que nous convertisserions en base64 par le biais d'une nouvelle classe `ImageConverter`.
