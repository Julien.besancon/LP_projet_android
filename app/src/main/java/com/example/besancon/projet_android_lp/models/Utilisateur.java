package com.example.besancon.projet_android_lp.models;

import android.os.Build;
import android.support.annotation.RequiresApi;

import java.util.Objects;

/** Classe modèle d'un utilisateur de l'application
 * Created by besancon on 19/05/17.
 */
public class Utilisateur
{

    private String id;
    private String login;
    private String motDePasse;

    public Utilisateur(){
        this.id = null;
        this.login = null;
        this.motDePasse = null;
    }

    public Utilisateur(String login, String motDePasse){
        this.id = null;
        this.login = login;
        this.motDePasse = motDePasse;
    }

    public Utilisateur(String id, String login, String motDePasse) {
        this.id = id;
        this.login = login;
        this.motDePasse = motDePasse;
    }

    public String getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public String getMotDePasse() {
        return motDePasse;
    }

    public void setId(String id){this.id=id;}

    public void setLogin(String login) {
        this.login = login;
    }

    public void setMotDePasse(String motDePasse) {
        this.motDePasse = motDePasse;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Utilisateur that = (Utilisateur) o;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            return Objects.equals(id, that.id) &&
                    Objects.equals(login, that.login) &&
                    Objects.equals(motDePasse, that.motDePasse);
        }
        return false;
    }

    @Override
    public int hashCode() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            return Objects.hash(id, login, motDePasse);
        }
        return 0;
    }

    @Override
    public String toString() {
        return "Utilisateur{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", motDePasse='" + motDePasse + '\'' +
                '}';
    }
}
