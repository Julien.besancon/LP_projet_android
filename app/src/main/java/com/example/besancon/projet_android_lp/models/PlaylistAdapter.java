package com.example.besancon.projet_android_lp.models;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.besancon.projet_android_lp.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;


/**
 * Created by Arthur on 06/03/2018.
 */

public class PlaylistAdapter extends ArrayAdapter<Playlist>{

    private ArrayList<Playlist> dataSet;
    private Context mContext;
    private int layoutRessourceId;

    private static class PlaylistViewHolder {
        TextView plName;
        TextView plDate;
        ImageView plImage;
        TextView plNombre;
    }

    public PlaylistAdapter(@NonNull Context context,ArrayList<Playlist> data, int layoutRessourceId) {
        super(context, layoutRessourceId, data);
        this.dataSet = data;
        this.mContext = context;
        this.layoutRessourceId = layoutRessourceId;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        PlaylistViewHolder viewHolder = null;

        if (convertView == null) {

            LayoutInflater inflater = ((Activity)mContext).getLayoutInflater();
            convertView = inflater.inflate(layoutRessourceId, parent, false);

            viewHolder = new PlaylistViewHolder();
            viewHolder.plName = (TextView) convertView.findViewById(R.id.tv_playlist_name);
            viewHolder.plDate = (TextView) convertView.findViewById(R.id.tv_playlist_date);
            viewHolder.plImage = (ImageView) convertView.findViewById(R.id.iv_playlist_image);
            viewHolder.plNombre = (TextView) convertView.findViewById(R.id.tv_playlist_number_of_music);

            convertView.setTag(viewHolder);

        } else {
            viewHolder = (PlaylistViewHolder) convertView.getTag();
        }

        Playlist playlist = dataSet.get(position);
        viewHolder.plName.setText(playlist.getName());
        viewHolder.plDate.setText("Créée le " + playlist.getDate());
        viewHolder.plImage.setImageResource(R.drawable.playlist256);
        viewHolder.plNombre.setText(playlist.getNb_music() + " entry(ies)");
        // Return the completed view to render on screen
        return convertView;
    }
}
