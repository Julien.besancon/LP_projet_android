package com.example.besancon.projet_android_lp.models;

import android.os.Build;

import java.util.Date;
import java.util.Objects;

/** Classe modèle d'un utilisateur de l'application
 * Created by besancon on 19/05/17.
 */
public class Playlist
{

    private String id_playlist;
    private String name;
    private String date;
    private String id_user;
    private String nb_music ;

    public Playlist(){
        this.id_playlist = null;
        this.name = null;
        this.date = null;
        this.id_user = null;
        this.nb_music = "0";
    }

    public Playlist(String name, String date){

        this.id_playlist = null;
        this.name = name;
        this.date = date;
        this.id_user = null;
    }

    public Playlist(String id_playlist, String name, String date, String nb_music ,String id_user) {
        this.id_playlist = id_playlist;
        this.name = name;
        this.date = date;
        this.id_user = id_user;
        this.nb_music = nb_music;
    }

    public String getId_playlist() {
        return id_playlist;
    }

    public Playlist setId_playlist(String id_playlist) {
        this.id_playlist = id_playlist;
        return this;
    }

    public String getName() {
        return name;
    }

    public Playlist setName(String name) {
        this.name = name;
        return this;
    }

    public String getDate() {
        return date;
    }

    public Playlist setDate(String date) {
        this.date = date;
        return this;
    }

    public String getId_user() {
        return id_user;
    }

    public Playlist setId_user(String id_user) {
        this.id_user = id_user;
        return this;
    }

    public String getNb_music() {
        return nb_music;
    }

    public Playlist setNb_music(String nb_music) {
        this.nb_music = nb_music;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Playlist that = (Playlist) o;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            return Objects.equals(id_playlist, that.id_playlist) &&
                    Objects.equals(id_user, that.id_user) &&
                    Objects.equals(name, that.name) &&
                    Objects.equals(date, that.date);
        }
        return false;
    }

    @Override
    public int hashCode() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            return Objects.hash(id_playlist, id_user, name, date);
        }
        return 0;
    }

    @Override
    public String toString() {
        return "Playlist{" +
                "id_playlist='" + this.id_playlist + '\'' +
                ", name='" + this.name + '\'' +
                ", Date='" + this.date + '\'' +
                ", NB music='" + this.nb_music + '\'' +
                ", id_user='" + this.id_user + '\'' +
                '}';
    }
}
