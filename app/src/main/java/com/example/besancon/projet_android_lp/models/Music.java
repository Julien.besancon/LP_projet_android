package com.example.besancon.projet_android_lp.models;

import android.os.Build;

import java.util.Date;
import java.util.Objects;

/** Classe modèle d'un utilisateur de l'application
 * Created by besancon on 19/05/17.
 */
public class Music
{

    private String id_music;
    private String title;
    private String image;
    private String id_playlist;

    public Music(){
        this.id_music = null;
        this.title = null;
        this.image = null;
        this.id_playlist = null;
    }

    public Music(String title, String date){
        this.id_music = null;
        this.title = title;
        this.image = null;
        this.id_playlist = null;
    }

    public Music(String id_music, String title, String image, String id_playlist) {
        this.id_music = id_music;
        this.title = title;
        this.image = image;
        this.id_playlist = id_playlist;
    }

    public String getId_music() {
        return id_music;
    }

    public void setId_music(String id_music) {
        this.id_music = id_music;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getId_playlist() {
        return id_playlist;
    }

    public void setId_playlist(String id_playlist) {
        this.id_playlist = id_playlist;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Music that = (Music) o;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            return Objects.equals(id_music, that.id_music) &&
                    Objects.equals(title, that.title) &&
                    Objects.equals(image, that.image) &&
                    Objects.equals(id_playlist, that.id_playlist);
        }
        return false;
    }

    @Override
    public int hashCode() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            return Objects.hash(id_music, title, image, id_playlist);
        }
        return 0;
    }

    @Override
    public String toString() {
        return "Playlist{" +
                "Id_music=" + this.id_music +
                ", Title='" + this.title + '\'' +
                ", Image='" + this.image + '\'' +
                ", Id_playlist='" + this.id_playlist + '\'' +
                '}';
    }
}
