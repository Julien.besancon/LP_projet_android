package com.example.besancon.projet_android_lp.models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.Date;


/**
 * Created by besancon on 17/01/18.
 */

public class DeezerBDDManager {

    private static final String NOM_BDD = ProjectSQLiteOpenHelper.NOM_BDD;
    private static final int VERSION_BDD = ProjectSQLiteOpenHelper.VERSION_BDD;

    private static final String ID_UTILISATEUR = ProjectSQLiteOpenHelper.ID_UTILISATEUR;
    private static final String LOGIN_UTILISATEUR = ProjectSQLiteOpenHelper.LOGIN_UTILISATEUR;
    private static final String MDP_UTILISATEUR = ProjectSQLiteOpenHelper.MDP_UTILISATEUR;

    public static final String TABLE_UTILISATEUR = ProjectSQLiteOpenHelper.TABLE_UTILISATEUR;
    private static final int UTILISATEUR_COL_ID = 0;
    private static final int UTILISATEUR_COL_LOGIN = 1;
    private static final int UTILISATEUR_COL_MDP = 2;

    private static final String ID_PLAYLIST = ProjectSQLiteOpenHelper.ID_PLAYLIST;
    private static final String NAME_PLAYLIST = ProjectSQLiteOpenHelper.NAME_PLAYLIST;
    private static final String NB_MUSIC_PLAYLIST = ProjectSQLiteOpenHelper.NB_MUSIC_PLAYLIST;
    private static final String DATE_CREATION_PLAYLIST = ProjectSQLiteOpenHelper.DATE_CREATION_PLAYLIST;

    public static final String TABLE_PLAYLIST = ProjectSQLiteOpenHelper.TABLE_PLAYLIST;
    private static final int PLAYLIST_COL_ID_PLAYLIST = 0;
    private static final int PLAYLIST_COL_NAME = 1;
    private static final int PLAYLIST_COL_DATE = 2;
    private static final int PLAYLIST_COL_ID_NB_MUSIC = 3;
    private static final int PLAYLIST_COL_ID_UTILISATEUR = 4;

    public static final String ID_MUSIC = ProjectSQLiteOpenHelper.ID_MUSIC;
    public static final String TITLE_MUSIC = ProjectSQLiteOpenHelper.TITLE_MUSIC;
    public static final String IMAGE_MUSIC = ProjectSQLiteOpenHelper.IMAGE_MUSIC;

    public static final String TABLE_MUSIC = ProjectSQLiteOpenHelper.TABLE_MUSIC;
    private static final int MUSIC_COL_ID_MUSIC = 0;
    private static final int MUSIC_COL_TITLE_MUSIC = 1;
    private static final int MUSIC_COL_IMAGE = 2;
    private static final int MUSIC_COL_ID_PLAYLIST = 3;

    private Context context;
    private SQLiteDatabase sqliteDatabase;
    private ProjectSQLiteOpenHelper projectSQLiteOpenHelper;

    public DeezerBDDManager(Context context) {
        this.context = context;
        this.projectSQLiteOpenHelper = new ProjectSQLiteOpenHelper(context, NOM_BDD, null, VERSION_BDD);
    }

    public void open() {
        //on ouvre la BDD en écriture
        this.sqliteDatabase = projectSQLiteOpenHelper.getWritableDatabase();
    }

    public void close() {
        //on ferme l'accès à la BDD
        this.sqliteDatabase.close();
    }

    public void remove(){
        this.projectSQLiteOpenHelper.remove(this.sqliteDatabase);
    }

    public void onCreate(){
        this.projectSQLiteOpenHelper.onCreate(this.sqliteDatabase);
    }

    public SQLiteDatabase getBDD() {
        return this.sqliteDatabase;
    }

    /* TABLE UTILISATEUR
    *
    *
    *
    *
    *
    *
    *
    * */

    public long insertUtilisateur(Utilisateur utilisateur) {
        //Création d'un ContentValues (fonctionne comme une HashMap)
        ContentValues values = new ContentValues();
        //on lui ajoute une valeur associé à une clé (qui est le nom de la colonne dans laquelle on veut mettre la valeur)
        values.put(ID_UTILISATEUR, utilisateur.getId());
        values.put(LOGIN_UTILISATEUR, utilisateur.getLogin());
        values.put(MDP_UTILISATEUR, utilisateur.getMotDePasse());
        //on insère l'objet dans la BDD via le ContentValues
        return this.sqliteDatabase.insert(TABLE_UTILISATEUR, null, values);
    }

    public Utilisateur getUtilisateurWithID(int id) {
        //Récupère dans un Cursor les valeur correspondant à un utilisateur contenu dans la BDD (ici on sélectionne l'utilisateur grâce à son login)
        Cursor c = this.sqliteDatabase.query(TABLE_UTILISATEUR, new String[]{ID_UTILISATEUR, LOGIN_UTILISATEUR, MDP_UTILISATEUR}, ID_UTILISATEUR + " = \"" + id + "\"", null, null, null, null);
        return this.cursorToUtilisateur(c);
    }

    public Utilisateur getUtilisateurWithLogin(String login) {
        //Récupère dans un Cursor les valeur correspondant à un utilisateur contenu dans la BDD (ici on sélectionne l'utilisateur grâce à son login)
        Cursor c = this.sqliteDatabase.query(TABLE_UTILISATEUR, new String[]{ID_UTILISATEUR, LOGIN_UTILISATEUR, MDP_UTILISATEUR}, LOGIN_UTILISATEUR + " = \"" + login + "\"", null, null, null, null);
        return this.cursorToUtilisateur(c);
    }

    public boolean inBDDUtilisateurWithLogin(String login) {
        Cursor c = this.sqliteDatabase.query(TABLE_UTILISATEUR, new String[]{ID_UTILISATEUR, LOGIN_UTILISATEUR, MDP_UTILISATEUR}, LOGIN_UTILISATEUR + " = \"" + login + "\"", null, null, null, null);
        return (this.cursorToUtilisateur(c)!=null);
    }


    //Cette méthode permet de convertir un cursor en un utilisateur
    private Utilisateur cursorToUtilisateur(Cursor c) {
        //si aucun élément n'a été retourné dans la requête, on renvoie null
        if (c.getCount() == 0)
            return null;

        //Sinon on se place sur le premier élément
        c.moveToFirst();
        //On créé un utilisateur "vierge"
        Utilisateur utilisateur = new Utilisateur();
        //on lui affecte toutes les infos grâce aux infos contenues dans le Cursor
        utilisateur.setId(c.getString(UTILISATEUR_COL_ID));
        utilisateur.setLogin(c.getString(UTILISATEUR_COL_LOGIN));
        utilisateur.setMotDePasse(c.getString(UTILISATEUR_COL_MDP));
        //On ferme le cursor
        c.close();
        //On retourne l'utilisateur
        return utilisateur;
    }

//        /* TABLE Playlist
//    *
//    *
//    *
//    *
//    *
//    *
//    *
//    * */

    public long insertPlaylist(Playlist playlist) {
        //Création d'un ContentValues (fonctionne comme une HashMap)
        ContentValues values = new ContentValues();
        //on lui ajoute une valeur associé à une clé (qui est le nom de la colonne dans laquelle on veut mettre la valeur)
        values.put(ID_PLAYLIST, playlist.getId_playlist());
        values.put(NAME_PLAYLIST, playlist.getName());
        values.put(DATE_CREATION_PLAYLIST, playlist.getDate());
        values.put(NB_MUSIC_PLAYLIST, playlist.getNb_music());
        values.put(ID_UTILISATEUR, String.valueOf(playlist.getId_user()));
        //on insère l'objet dans la BDD via le ContentValues
        return this.sqliteDatabase.insert(TABLE_PLAYLIST, null, values);
    }

    public int updatePlaylist(String id, Playlist playlist) {
        ContentValues values = new ContentValues();
        values.put(NAME_PLAYLIST, playlist.getName());
        values.put(DATE_CREATION_PLAYLIST, playlist.getDate());
        values.put(NB_MUSIC_PLAYLIST, playlist.getNb_music());
        return this.sqliteDatabase.update(TABLE_PLAYLIST, values, ID_PLAYLIST + " = " + id, null);
    }

    public int removePlaylistWithID(String id_playlist) {
        this.sqliteDatabase.delete(TABLE_MUSIC, ID_PLAYLIST+ " = " + id_playlist, null);
        return this.sqliteDatabase.delete(TABLE_PLAYLIST, ID_PLAYLIST+ " = " + id_playlist, null);
    }

    public ArrayList<Playlist> getAllPlaylistFromIdDeezer(String id_user){

        Cursor c = this.sqliteDatabase.query(TABLE_PLAYLIST,new String[]{ID_PLAYLIST,NAME_PLAYLIST,DATE_CREATION_PLAYLIST,NB_MUSIC_PLAYLIST,ID_UTILISATEUR},ID_UTILISATEUR + " = " + id_user,null,null,null,null);
        return this.cursorToListePlaylist(c);

    }

    public boolean inBDDPlaylistById(Playlist playlist) {
        ArrayList<Playlist> listPlaylist = getAllPlaylistFromIdDeezer(playlist.getId_user());
        for(Playlist playlistbdd : listPlaylist){
            if(playlistbdd.getId_playlist().equals(playlist.getId_playlist())){
                return true;
            }
        }
        return false;
    }


    private ArrayList<Playlist> cursorToListePlaylist(Cursor c){
        // Recupere toutes les histoires d'un curseur d'ArrayList

        ArrayList<Playlist> playlists = new ArrayList<>();
        Playlist p;
        while (c.moveToNext()) {
            p = new Playlist(
                    c.getString(PLAYLIST_COL_ID_PLAYLIST),
                    c.getString(PLAYLIST_COL_NAME),
                    c.getString(PLAYLIST_COL_DATE),
                    c.getString(PLAYLIST_COL_ID_NB_MUSIC),
                    c.getString(PLAYLIST_COL_ID_UTILISATEUR)
            );

            playlists.add(p);
        }
        return playlists;
    }

//     /* TABLE Music
//    *
//    *
//    *
//    *
//    *
//    *
//    *
//    * */

    public long insertMusic(Music music) {
        //Création d'un ContentValues (fonctionne comme une HashMap)
        ContentValues values = new ContentValues();
        //on lui ajoute une valeur associé à une clé (qui est le nom de la colonne dans laquelle on veut mettre la valeur)
        values.put(ID_MUSIC, music.getId_music());
        values.put(TITLE_MUSIC, music.getTitle());
        values.put(IMAGE_MUSIC, String.valueOf(music.getImage()));
        values.put(ID_PLAYLIST, String.valueOf(music.getId_playlist()));
        //on insère l'objet dans la BDD via le ContentValues
        return this.sqliteDatabase.insert(TABLE_MUSIC, null, values);
    }

    public int removeMusicInPlaylist(String id_playlist,String id_music){
        return this.sqliteDatabase.delete(TABLE_MUSIC, ID_PLAYLIST+ " = " + id_playlist + " AND " + ID_MUSIC + " = " + id_music, null);
    }

    public ArrayList<Music> getAllMusicFromIdPlaylist(String id_playlist){

        Cursor c = this.sqliteDatabase.query(TABLE_MUSIC,new String[]{ID_MUSIC,TITLE_MUSIC,IMAGE_MUSIC,ID_PLAYLIST},ID_PLAYLIST + " = " + id_playlist,null,null,null,null);
        return this.cursorToListeMusic(c);

    }

    public boolean inBDDMusicById(Music music) {
        ArrayList<Music> listPlaylist = getAllMusicFromIdPlaylist(music.getId_playlist());
        for(Music musicbdd : listPlaylist){
            if(musicbdd.getId_playlist().equals(music.getId_playlist()) && musicbdd.getId_music().equals(music.getId_music())){
                return true;
            }
        }
        return false;
    }


    private ArrayList<Music> cursorToListeMusic(Cursor c){

        ArrayList<Music> musics = new ArrayList<>();
        Music m;
        while (c.moveToNext()) {
            m = new Music(
                    c.getString(MUSIC_COL_ID_MUSIC),
                    c.getString(MUSIC_COL_TITLE_MUSIC),
                    c.getString(MUSIC_COL_IMAGE),
                    c.getString(MUSIC_COL_ID_PLAYLIST)
            );

            musics.add(m);
        }
        return musics;
    }



}
