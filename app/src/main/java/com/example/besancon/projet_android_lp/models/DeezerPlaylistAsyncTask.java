package com.example.besancon.projet_android_lp.models;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.besancon.projet_android_lp.MusicsActivity;
import com.example.besancon.projet_android_lp.PlaylistsActivity;
import com.example.besancon.projet_android_lp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by dalbis on 24/01/18.
 */

public class DeezerPlaylistAsyncTask extends AsyncTask<URL, Void, JSONObject> {
    private static final String TAG = "DeezerPlaylistAsyncTask";
    private AppCompatActivity context;
    public JSONObject jsonresponse;
    public ArrayList<Playlist> listPlaylist;
    private ListView playlistListView;

    public DeezerPlaylistAsyncTask(AppCompatActivity context) {
        super();
        this.context = context;
        this.listPlaylist = new ArrayList<Playlist>();
    }

    @Override
    protected JSONObject doInBackground(URL... params) {
        HttpURLConnection connection = null;

        try{
            connection = (HttpURLConnection) params[0].openConnection();
            int response = connection.getResponseCode();

            if (response == HttpURLConnection.HTTP_OK) {
                StringBuilder builder = new StringBuilder();

                try{
                    BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    String line;

                    while ((line = reader.readLine()) != null) {
                        builder.append(line);
                    }

                }catch (IOException e) {
                    Log.e(TAG, "Erreur IOException ! \n");
                    e.printStackTrace();
                    Toast toast = Toast.makeText(this.context, "Erreur lors de la lecture des données reçues", Toast.LENGTH_LONG);
                    toast.show();
                }
                return new JSONObject(builder.toString());

            }else {
                Log.e(TAG, "Erreur de connection !\n");
                Toast toast = Toast.makeText(this.context, "Erreur : La connexion n'a pas pu être établie", Toast.LENGTH_LONG);
                toast.show();
            }
        }catch (Exception e) {
            Log.e(TAG, "Une Exception inconnue a été levée");
            e.printStackTrace();
            Toast toast = Toast.makeText(this.context, "Erreur : La connexion n'a pas pu être établie", Toast.LENGTH_LONG);
            toast.show();
        }finally {
            connection.disconnect(); // fermeture HttpURLConnection
        }
        JSONObject jso = null;
        try {
            jso = new JSONObject("{'haha': 'haha ça marche pas'}");
        }catch(Exception e){
            e.printStackTrace();
        }
        return jso;
    }

    //récupération de l'objet json
    @Override
    protected void onPostExecute(JSONObject json) {
        this.jsonresponse = json;
        Log.i("montest",json.toString());

        try {
            JSONArray values = (JSONArray) json.get("data");

            for(int i = 0; i<values.length(); i++){
                Playlist playlist = new Playlist();
                JSONObject value = (JSONObject) values.get(i);
                playlist.setId_playlist(value.get("id").toString());
                playlist.setName(value.get("title").toString());
                playlist.setNb_music(value.get("nb_tracks").toString());
//                création date
                playlist.setDate(value.get("creation_date").toString().substring(0,11));
                playlist.setId_user(((JSONObject)value.get("creator")).get("id").toString());
                this.listPlaylist.add(playlist);
                Log.i("montest",playlist.toString());

                updateBdd(playlist);
            }

            if(this.listPlaylist.size()> 0){
                removePlaylistInBdd();
            }
            Toast toast = Toast.makeText(this.context, "La base de données a été mise à jour", Toast.LENGTH_LONG);
            toast.show();

            playlistListView = (ListView) this.context.findViewById(R.id.lv_playlist);
            PlaylistAdapter playlistAdapter = new PlaylistAdapter(this.context, this.listPlaylist, R.layout.row_item_playlist);
            playlistListView.setAdapter(playlistAdapter);

            playlistListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Playlist p = (Playlist)playlistListView.getItemAtPosition(position);
                    Intent intent = new Intent (context, MusicsActivity.class);
                    intent.putExtra("id_playlist", p.getId_playlist());
                    intent.putExtra("id_deezer", p.getId_user());
                    context.setResult(Activity.RESULT_OK , intent );
                    context.startActivityForResult(intent,1);
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void updateBdd(Playlist playlist)
    {
        DeezerBDDManager deezerBDDManager = new DeezerBDDManager(this.context);
        deezerBDDManager.open();
        if(deezerBDDManager.inBDDPlaylistById(playlist)){
            Log.i("montest","MAJ");
            deezerBDDManager.updatePlaylist(playlist.getId_playlist(),playlist);
        }
        else{
            Log.i("montest","INSERT");
            deezerBDDManager.insertPlaylist(playlist);
        }
        deezerBDDManager.close();
    }

    public void removePlaylistInBdd()
    {
        String id_user = listPlaylist.get(0).getId_user();
        DeezerBDDManager deezerBDDManager = new DeezerBDDManager(this.context);
        deezerBDDManager.open();
        Log.i("montest","avant appel");

        ArrayList<Playlist> listPlaylistDBB = deezerBDDManager.getAllPlaylistFromIdDeezer(id_user);
        Log.i("montest","apreès appel");

        for(Playlist playlistBDD : listPlaylistDBB){
            if(!inNewListPlaylist(playlistBDD)){
                deezerBDDManager.removePlaylistWithID(playlistBDD.getId_playlist());
            }
        }
        Log.i("montest","apreès for ");

        deezerBDDManager.close();
    }

    public boolean inNewListPlaylist(Playlist playlistBDD){
        for (Playlist playlist:this.listPlaylist){
            if(playlist.getId_playlist().equals(playlistBDD.getId_playlist())){
                return true;
            }
        }
        return false;
    }
}
