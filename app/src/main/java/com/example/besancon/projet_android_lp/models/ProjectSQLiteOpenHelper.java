package com.example.besancon.projet_android_lp.models;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.os.Build;
import android.util.Log;


/**
 * Created by besancon on 14/12/17.
 */

public class ProjectSQLiteOpenHelper extends SQLiteOpenHelper {

    public static final String NOM_BDD = "Deezer.db";
    public static final int VERSION_BDD = 1;

    //Début Table utilisateur ***************************************************************************************
    public static final String TABLE_UTILISATEUR = "UTILISATEUR";
    public static final String ID_UTILISATEUR = "id_utilisateur";
    public static final String LOGIN_UTILISATEUR = "login_utilisateur";
    public static final String MDP_UTILISATEUR = "mdp_utilisateur";

    public static final String CREATE_TABLE_UTILISATEUR =
            "CREATE TABLE IF NOT EXISTS "+TABLE_UTILISATEUR+
                    " ( " +
                    ID_UTILISATEUR+" VARCHAR(32) PRIMARY KEY,"+
                    LOGIN_UTILISATEUR+" VARCHAR(32),"+
                    MDP_UTILISATEUR+" VARCHAR(32)"+
                    ");";

    public static final String DROP_TABLE_UTILISATEUR = "DROP TABLE IF EXISTS "+TABLE_UTILISATEUR+";";
    //Fin Table utilisateur ***************************************************************************************

    //Début Table playlist ***************************************************************************************
    public static final String TABLE_PLAYLIST = "PLAYLIST";
    public static final String ID_PLAYLIST = "id_playlist";
    public static final String NAME_PLAYLIST = "nom_playlist";
    public static final String NB_MUSIC_PLAYLIST = "nb_music_playlist";
    public static final String DATE_CREATION_PLAYLIST = "date_creation_playlist";
    public static final String CREATE_TABLE_PLAYLIST =
            "CREATE TABLE IF NOT EXISTS "+TABLE_PLAYLIST+
                    " ( " +
                    ID_PLAYLIST+" VARCHAR(255) PRIMARY KEY,"+
                    NAME_PLAYLIST+" VARCHAR(255),"+
                    DATE_CREATION_PLAYLIST+" VARCHAR(255),"+
                    NB_MUSIC_PLAYLIST+" VARCHAR(10),"+
                    ID_UTILISATEUR+" VARCHAR(255),"+
                    "FOREIGN KEY ("+ID_UTILISATEUR+") REFERENCES "+TABLE_UTILISATEUR+"("+ID_UTILISATEUR+")" +
                    ");";
    public static final String DROP_TABLE_PLAYLIST = "DROP TABLE IF EXISTS "+TABLE_PLAYLIST+";";
    //Fin Table playlist ***************************************************************************************

    //Début Table music***************************************************************************************
    public static final String TABLE_MUSIC = "MUSIC";
    public static final String ID_MUSIC = "id_music";
    public static final String TITLE_MUSIC = "title_music";
    public static final String IMAGE_MUSIC = "image_music";
    public static final String CREATE_TABLE_MUSIC =
            "CREATE TABLE IF NOT EXISTS "+TABLE_MUSIC+
                    " ( " +
                    ID_MUSIC+" VARCHAR(255),"+
                    TITLE_MUSIC+" VARCHAR(255),"+
                    IMAGE_MUSIC+" VARCHAR(255),"+
                    ID_PLAYLIST+" VARCHAR(255),"+
                    "PRIMARY KEY ("+ID_MUSIC+", "+ID_PLAYLIST+")," +
                    "FOREIGN KEY ("+ID_PLAYLIST+") REFERENCES "+TABLE_PLAYLIST+"("+ID_PLAYLIST+")" +
                    ");";
    public static final String DROP_TABLE_MUSIC = "DROP TABLE IF EXISTS "+TABLE_MUSIC+";";
    //Fin Table music ***************************************************************************************


    public ProjectSQLiteOpenHelper(Context context, String name, CursorFactory cursorFactory, int version){
        super(context, name, cursorFactory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE_UTILISATEUR);
        sqLiteDatabase.execSQL(CREATE_TABLE_PLAYLIST);
        sqLiteDatabase.execSQL(CREATE_TABLE_MUSIC);
    }

    @Override
    public void onOpen(SQLiteDatabase sqLiteDatabase) {
        super.onOpen(sqLiteDatabase);
    }

    @Override
    public void onConfigure(SQLiteDatabase sqLiteDatabase) {
        super.onConfigure(sqLiteDatabase);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            sqLiteDatabase.setForeignKeyConstraintsEnabled(true);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(DROP_TABLE_MUSIC);
        sqLiteDatabase.execSQL(DROP_TABLE_PLAYLIST);
        sqLiteDatabase.execSQL(DROP_TABLE_UTILISATEUR);
        onCreate(sqLiteDatabase);
    }

    public void remove(SQLiteDatabase sqLiteDatabase){
        sqLiteDatabase.execSQL(DROP_TABLE_MUSIC);
        sqLiteDatabase.execSQL(DROP_TABLE_PLAYLIST);
        sqLiteDatabase.execSQL(DROP_TABLE_UTILISATEUR);
    }

}
