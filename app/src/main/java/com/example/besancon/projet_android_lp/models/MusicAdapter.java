package com.example.besancon.projet_android_lp.models;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.besancon.projet_android_lp.R;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by Arthur on 08/03/2018.
 */

public class MusicAdapter extends ArrayAdapter<Music> {

    private ArrayList<Music> dataSet;
    private Context mContext;
    private int layoutRessourceId;

    private static class MusicViewHolder {
        TextView mName;
//        TextView mDate;
        ImageView mImage;
    }

    public MusicAdapter(@NonNull Context context, ArrayList<Music> data, int layoutRessourceId) {
        super(context, layoutRessourceId, data);
        this.dataSet = data;
        this.mContext = context;
        this.layoutRessourceId = layoutRessourceId;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MusicAdapter.MusicViewHolder viewHolder = null;

        if (convertView == null) {

            LayoutInflater inflater = ((Activity)mContext).getLayoutInflater();
            convertView = inflater.inflate(layoutRessourceId, parent, false);

            viewHolder = new MusicAdapter.MusicViewHolder();
            viewHolder.mName = (TextView) convertView.findViewById(R.id.tv_music_name);
//            viewHolder.mDate = (TextView) convertView.findViewById(R.id.tv_music_date);
            viewHolder.mImage = (ImageView) convertView.findViewById(R.id.iv_music_image);

            convertView.setTag(viewHolder);

        } else {
            viewHolder = (MusicAdapter.MusicViewHolder) convertView.getTag();
        }

        Music music = dataSet.get(position);
        viewHolder.mName.setText(music.getTitle());
//        viewHolder.mDate.setText("Créée le " +
//                new SimpleDateFormat("dd-MM-yyyy").format(music.getDate()));
        // Return the completed view to render on screen
        Picasso.with(viewHolder.mImage.getContext()).load(music.getImage()).centerCrop().fit().into(viewHolder.mImage);
        return convertView;
    }
}
