package com.example.besancon.projet_android_lp;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.besancon.projet_android_lp.models.DeezerBDDManager;
import com.example.besancon.projet_android_lp.models.Utilisateur;


public class InscriptionActivity extends AppCompatActivity {

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_inscription);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(false);
    }

    public void validerInscription(View view){
        EditText idDeezer = (EditText)findViewById(R.id.editIdentifiantDeezer);
        EditText login = (EditText)findViewById(R.id.editLogin);
        EditText password = (EditText)findViewById(R.id.editPassword);
        EditText passwordConfirm = (EditText)findViewById(R.id.editPasswordCondfirm);
        String textIdDeezer = idDeezer.getText().toString();
        String textLogin = login.getText().toString();
        String textPassword = password.getText().toString();

        DeezerBDDManager deezerBDDManager = new DeezerBDDManager(this);
        deezerBDDManager.open();
        if(!password.getText().toString().equals(passwordConfirm.getText().toString()))
        {
            Toast toast = Toast.makeText(this, "le mot de passe est mal confirmé", Toast.LENGTH_LONG);
            toast.show();
        }
        else if (deezerBDDManager .inBDDUtilisateurWithLogin(textLogin)){
            Toast toast = Toast.makeText(this, "Ce nom existe déjà", Toast.LENGTH_LONG);
            toast.show();
        }
        else if (!textIdDeezer.matches("^\\p{Digit}+$")){
            Toast toast = Toast.makeText(this, "L'id Deezer doit être un nombre", Toast.LENGTH_LONG);
            toast.show();
        }
        else
        {
            Utilisateur utilisateur = new Utilisateur(textIdDeezer,textLogin,textPassword);
            deezerBDDManager.insertUtilisateur(utilisateur);
            deezerBDDManager.close();
            setResult(RESULT_OK);
            super.finish ();
        }

    }

    // dans le cas ou l'on appuie sur la fleche de retour
    @Override
    public void finish ()
    {
        super.finish ();
    }
}
