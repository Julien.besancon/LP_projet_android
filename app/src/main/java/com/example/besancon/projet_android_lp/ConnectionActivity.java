package com.example.besancon.projet_android_lp;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.besancon.projet_android_lp.models.DeezerBDDManager;
import com.example.besancon.projet_android_lp.models.Music;
import com.example.besancon.projet_android_lp.models.Playlist;
import com.example.besancon.projet_android_lp.models.Utilisateur;

import java.util.Date;


public class ConnectionActivity extends AppCompatActivity {

    String idUser;
    DeezerBDDManager deezerBDDManager;
    SharedPreferences perso;
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_connection);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(false);

        this.deezerBDDManager = new DeezerBDDManager(this);
        this.deezerBDDManager.open();
//        this.deezerBDDManager.remove();
        this.deezerBDDManager.onCreate();
        if(this.deezerBDDManager.getUtilisateurWithID(387246455) == null){
            addDataInBDD();
        }

        perso = getPreferences(MODE_PRIVATE);
        if(perso.getString("login",null)!=null){
            Utilisateur user = deezerBDDManager.getUtilisateurWithLogin(perso.getString("login",null));
            ((EditText)findViewById(R.id.txtinput_login)).setText(user.getLogin());
            ((EditText)findViewById(R.id.txtinput_password)).setText(user.getMotDePasse());
        }
        this.deezerBDDManager.close();

    }

    public void onLogInClick(View view){

        String login = ((EditText)findViewById(R.id.txtinput_login)).getText().toString();
        String password = ((EditText)findViewById(R.id.txtinput_password)).getText().toString();
        if(checkCredentials(login, password)){
            perso.edit().putString("login",login).commit();
            Intent intent = new Intent (this,PlaylistsActivity.class);
            intent.putExtra("id_deezer", idUser);
            setResult(Activity.RESULT_OK , intent );
            startActivity(intent);
        }
        else{
            Toast toast = Toast.makeText(this, "Votre login ou votre mot de passe n'est pas correct", Toast.LENGTH_LONG);
            toast.show();
        }
    }

    public boolean checkCredentials(String login, String password){
        this.deezerBDDManager.open();

        Utilisateur utilisateur = deezerBDDManager.getUtilisateurWithLogin(login);
        if (utilisateur!=null)
            idUser = utilisateur.getId();

        this.deezerBDDManager.close();

        return utilisateur != null && utilisateur.getMotDePasse().equals(password);
    }

    public void inscription(View view){
        Intent intent = new Intent(this,InscriptionActivity.class);
        startActivityForResult(intent,1);
    }

    @Override
    public void onActivityResult (int requestcode, int resultcode, Intent intent ) {

        super.onActivityResult(requestcode, resultcode, intent);

        switch(requestcode){


            case 1 : // RETOUR DE L'INSCRITPION
                switch(resultcode){
                    case RESULT_OK :
                        Toast.makeText(this, "Votre compte a bien été créé.", Toast.LENGTH_SHORT).show();
                        break;
                    case RESULT_CANCELED :
                        Toast.makeText(this, "Inscription annulée", Toast.LENGTH_SHORT).show();
                        break;
                    default :
                        Toast.makeText(this, "Une anomalie est survenue. Veuillez refaire votre inscription.", Toast.LENGTH_SHORT).show();
                        break;
                }
                break;



            case 2 : // RETOUR DE LA DECONNEXION
                switch(resultcode){
                    case RESULT_OK :
                        String message = intent.getStringExtra("deco");
                        Toast toast = Toast.makeText(this, message, Toast.LENGTH_LONG);
                        toast.show();
                        break;
                    case RESULT_CANCELED :
                        Toast.makeText(this, "Une anomalie est survenue. Vous n'êtes pas deconnecté", Toast.LENGTH_SHORT).show();
                        break;
                    default: break;
                }
                break;
        }
    }

    @Override
    protected void onDestroy() {
        this.deezerBDDManager.close();
        super.onDestroy();
    }

    public void addDataInBDD(){
        //add user
        Utilisateur utilisateur = new Utilisateur("12855440","pa","a");
        deezerBDDManager.insertUtilisateur(utilisateur);

        utilisateur.setId("387246455");
        utilisateur.setLogin("ju");
        utilisateur.setMotDePasse("ju");
        deezerBDDManager.insertUtilisateur(utilisateur);

    }

}
