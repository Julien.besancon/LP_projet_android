package com.example.besancon.projet_android_lp.models;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.besancon.projet_android_lp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


/**
 * Created by dalbis on 24/01/18.
 */

public class DeezerMusicAsyncTask extends AsyncTask<URL, Void, JSONObject> {
    private static final String TAG = "DeezerMusicAsyncTask";
    private AppCompatActivity context;
    public JSONObject jsonresponse;
    public ArrayList<Music> listMusic;
    private ListView musicListView;

    public DeezerMusicAsyncTask(AppCompatActivity context) {
        super();
        this.context = context;
        this.listMusic = new ArrayList<Music>();
    }

    @Override
    protected JSONObject doInBackground(URL... params) {
        HttpURLConnection connection = null;

        try{
            connection = (HttpURLConnection) params[0].openConnection();
            int response = connection.getResponseCode();

            if (response == HttpURLConnection.HTTP_OK) {
                StringBuilder builder = new StringBuilder();

                try{
                    BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    String line;

                    while ((line = reader.readLine()) != null) {
                        builder.append(line);
                    }

                }catch (IOException e) {
                    Log.e(TAG, "Erreur IOException ! \n");
                    e.printStackTrace();
                    Toast toast = Toast.makeText(this.context, "Erreur lors de la lecture des données reçues", Toast.LENGTH_LONG);
                    toast.show();
                }
                return new JSONObject(builder.toString());

            }else {
                Log.e(TAG, "Erreur de connection !\n");
                Toast toast = Toast.makeText(this.context, "Erreur : La connexion n'a pas pu être établie", Toast.LENGTH_LONG);
                toast.show();
            }
        }catch (Exception e) {
            Log.e(TAG, "Une Exception inconnue a été levée");
            e.printStackTrace();
            Toast toast = Toast.makeText(this.context, "Erreur : La connexion n'a pas pu être établie", Toast.LENGTH_LONG);
            toast.show();
        }finally {
            connection.disconnect(); // fermeture HttpURLConnection
        }
        JSONObject jso = null;
        try {
            jso = new JSONObject("{'haha': 'haha ça marche pas'}");
        }catch(Exception e){
            e.printStackTrace();
        }
        return jso;
    }

    //récupération de l'objet json
    @Override
    protected void onPostExecute(JSONObject json) {
        try {
//            Log.i("montest","Je vais mettre à jour");
            String id_playlist = json.get("id").toString();

            JSONArray values = (JSONArray)((JSONObject)json.get("tracks")).get("data");

            for(int i = 0; i<values.length(); i++){
                Music music = new Music();
                JSONObject value = (JSONObject) values.get(i);
                music.setId_music(value.get("id").toString());
                music.setTitle(value.get("title").toString());
                music.setId_playlist(id_playlist);
//                création date
                music.setImage(((JSONObject)value.get("album")).get("cover_medium").toString());
                this.listMusic.add(music);
                updateBdd(music);
//                Log.i("montest",music.toString());
            }

            if(this.listMusic.size()> 0){
                removeMusicInBdd(id_playlist);
            }

            Toast toast = Toast.makeText(this.context, "La base de données a été mise à jour", Toast.LENGTH_LONG);
            toast.show();

            musicListView = (ListView) this.context.findViewById(R.id.lv_music);
            MusicAdapter musicAdapter = new MusicAdapter(this.context, this.listMusic, R.layout.row_item_music);
            musicListView.setAdapter(musicAdapter);


            // Si la listview est vide au départ puis remplie le setonitemclicklistener de l'activitye s'applique pas.
            // Il faut donc le réappliquer au cas ou.
            musicListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Music p = (Music)musicListView.getItemAtPosition(position);
                    String url = "https://www.deezer.com/fr/track/"+p.getId_music();
                    Intent intent = new Intent( Intent.ACTION_VIEW, Uri.parse( url ) );
                    context.startActivity(intent);
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }

        this.jsonresponse = json;
        Log.i(TAG, json.toString());
    }

    public void updateBdd(Music music)
    {
        DeezerBDDManager deezerBDDManager = new DeezerBDDManager(this.context);
        deezerBDDManager.open();
        if(!deezerBDDManager.inBDDMusicById(music)){
            deezerBDDManager.insertMusic(music);
        }
        deezerBDDManager.close();
    }

    public void removeMusicInBdd(String id_playlist)
    {
        DeezerBDDManager deezerBDDManager = new DeezerBDDManager(this.context);
        deezerBDDManager.open();

        ArrayList<Music> listMusicDBB = deezerBDDManager.getAllMusicFromIdPlaylist(id_playlist);

        for(Music musicBDD : listMusicDBB){
            if(!inNewListPlaylist(musicBDD)){
                deezerBDDManager.removeMusicInPlaylist(musicBDD.getId_playlist(),musicBDD.getId_music());
            }
        }
        deezerBDDManager.close();
    }

    public boolean inNewListPlaylist(Music musicBDD){
        for (Music music : this.listMusic){
            if(music.getId_music().equals(musicBDD.getId_music()) && music.getId_playlist().equals(musicBDD.getId_playlist())){
                return true;
            }
        }
        return false;
    }

}
