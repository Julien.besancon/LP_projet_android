package com.example.besancon.projet_android_lp;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.Button;
import android.widget.AdapterView;
import android.widget.Toast;

import com.example.besancon.projet_android_lp.models.DeezerBDDManager;
import com.example.besancon.projet_android_lp.models.DeezerMusicAsyncTask;
import com.example.besancon.projet_android_lp.models.Music;
import com.example.besancon.projet_android_lp.models.MusicAdapter;

import java.net.URL;
import java.util.ArrayList;

public class MusicsActivity extends AppCompatActivity {

    String id_playlist;
    String id_deezer;
    ArrayList<Music> listeMusic;
    DeezerBDDManager deezerBDDManager;
    DeezerMusicAsyncTask data;
    ListView musicListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_musics);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(false);

        Intent intent = getIntent ();

        listeMusic = new ArrayList<Music>();
        this.id_playlist = intent.getStringExtra("id_playlist");
        this.id_deezer = intent.getStringExtra("id_deezer");
        Toast toast = Toast.makeText(this, "id_playlist : " + this.id_playlist, Toast.LENGTH_LONG);
        toast.show();

        deezerBDDManager = new DeezerBDDManager(this);
        deezerBDDManager.open();
        listeMusic = deezerBDDManager.getAllMusicFromIdPlaylist(this.id_playlist);
        Log.i("montest",listeMusic.toString());

        if(listeMusic.size() > 0){
            for(Music music: listeMusic){
                Log.i("montest",music.toString());
            }

            musicListView = (ListView) findViewById(R.id.lv_music);
            MusicAdapter musicAdapter = new MusicAdapter(this, listeMusic, R.layout.row_item_music);
            musicListView.setAdapter(musicAdapter);

            musicListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Music p = (Music)musicListView.getItemAtPosition(position);
                    String url = "https://www.deezer.com/fr/track/"+p.getId_music();
                    Intent intent = new Intent( Intent.ACTION_VIEW, Uri.parse( url ) );
                    startActivity(intent);
                }
            });
        }

        deezerBDDManager.close();
    }


    public void getJsonData(View v) {
        try {
            data = new DeezerMusicAsyncTask(this);
            data.execute(new URL("https://api.deezer.com/playlist/" + this.id_playlist));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void finish ()
    {
        Intent intent = new Intent ();
        intent.putExtra("id_deezer", this.id_deezer);
        super.finish ();
    }

}
