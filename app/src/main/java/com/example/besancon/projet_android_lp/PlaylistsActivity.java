package com.example.besancon.projet_android_lp;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.besancon.projet_android_lp.models.DeezerBDDManager;
import com.example.besancon.projet_android_lp.models.DeezerPlaylistAsyncTask;
import com.example.besancon.projet_android_lp.models.Playlist;
import com.example.besancon.projet_android_lp.models.PlaylistAdapter;


import java.net.URL;
import java.util.ArrayList;

/*
1 afficher le contenue de la bdd
2 dans le post execute prendre l'arraylist updaté et la remplacer
 */
public class PlaylistsActivity extends AppCompatActivity {

    public String id_deezer;
    ArrayList<Playlist> listePlay;
    DeezerBDDManager deezerBDDManager;
    DeezerPlaylistAsyncTask data;
    ListView playlistListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_playlists);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(false);

        Intent intent = getIntent ();
        this.id_deezer = intent.getStringExtra("id_deezer");
        listePlay = new ArrayList<Playlist>();

        deezerBDDManager = new DeezerBDDManager(this);
        deezerBDDManager.open();
        listePlay = deezerBDDManager.getAllPlaylistFromIdDeezer(this.id_deezer);

        if(listePlay.size() > 0){
            for(Playlist playlist: listePlay){
                Log.i("montest",playlist.toString());
            }

            playlistListView = (ListView) findViewById(R.id.lv_playlist);
            PlaylistAdapter playlistAdapter = new PlaylistAdapter(this, listePlay, R.layout.row_item_playlist);
            playlistListView.setAdapter(playlistAdapter);

            playlistListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Playlist p = (Playlist)playlistListView.getItemAtPosition(position);
                    Intent intent = new Intent (PlaylistsActivity.this, MusicsActivity.class);
                    intent.putExtra("id_playlist", p.getId_playlist());
                    intent.putExtra("id_deezer", p.getId_user());
                    setResult(Activity.RESULT_OK , intent );
                    startActivityForResult(intent,1);
                }
            });

        }

        deezerBDDManager.close();
    }

    public void getJsonData(View v){
        try {
            data = new DeezerPlaylistAsyncTask(this);
            data.execute(new URL("https://api.deezer.com/user/"+this.id_deezer+"/playlists"));

        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void finish ()
    {
        super.finish ();
    }

}
